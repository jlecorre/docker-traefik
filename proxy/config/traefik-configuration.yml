# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Traefik static configuration
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Available arguments: https://doc.traefik.io/traefik/reference/static-configuration/cli/

# Docker configuration backend
providers:
  # Wait N seconds between two consecutives reload of the configuration
  providersThrottleDuration: 2s
  # Use docker provider to detect changes to apply
  docker:
    # Default docker network to use for connections to all containers
    network: frontend
    # Do not expose by default the new created containers
    # /!\ With this flag, it's required to enable this label on container to publicly expose
    # => traefik.enable=true
    exposedByDefault: false
    # Set Traefik to use tcp docker's endpoint instead the Docker daemon socket
    # For more information: https://doc.traefik.io/traefik/providers/docker/#docker-api-access
    # Some iptables rules need to be enabled to allow traffic on this endpoint depunding on your configuration
    endpoint: tcp://${DOMAIN}.${TLD}:2376
    # Use TLS secured connection to communicate with the Docker daemon
    # These TLS certificates are configured with a SNI composed by the previous configured endpoint
    tls:
      ca: /etc/traefik/certs/ca.pem
      cert: /etc/traefik/certs/cert.pem
      key: /etc/traefik/certs/key.pem
      # Do not accepts any certificate presented by the server
      insecureSkipVerify: false
      # false => require and verify client cert
      # true => verify client cert if given
      caOptional: false
    # Watch the Docker daemon event in live (and not the Swarm event as described in the Traefik's documentation)
    watch: true
    # Disable the Swarm mode
    swarmMode: false

# Access logs configuration
accessLog:
  filePath: "/var/log/traefik/access.log"
  format: "common" # Available value: json|common

# Logs configuration
# Available log levels: ERROR | DEBUG | PANIC | FATAL | WARN | INFO
log:
  filePath: "/var/log/traefik/traefik.log"
  format: "common" # Available value: json
  level: INFO

# API and dashboard configuration
api:
  insecure: false
  dashboard: true
  debug: false

# Default entrypoints configuration
entryPoints:
  web:
    # To define depending of the values declared in the .env file
    address: ":XXXXX" # TCP is used by default
    http:
      redirections:
        entryPoint:
          to: websecure
          scheme: https
          permanent: true
          priority: 1
  websecure:
    # To define depending of the values declared in the .env file
    address: ":XXXXX" # TCP is used by default
  dashboard:
    # To define depending of the values declared in the .env file
    address: ":XXXXX" # TCP is used by default

# TLS configuration
certificatesResolvers:
  # Resolver used to debug services
  letsEncryptStaging:
    acme:
      email: "${USERNAME}@${DOMAIN}.${TLD}"
      storage: "/etc/traefik/acme.json"
      # Staging server to use during debugging steps
      caServer: "https://acme-staging-v02.api.letsencrypt.org/directory"
      keyType: "RSA4096"
      # DNS challenge is executed through the Gandi provider
      dnsChallenge:
        provider: "gandiv5"
        resolvers:
          - "ns-166-a.gandi.net"
          - "ns-189-b.gandi.net"
          - "ns-89-c.gandi.net"
  # Resolver used to production services
  letsEncryptProduction:
    acme:
      email: "${USERNAME}@${DOMAIN}.${TLD}"
      storage: "/etc/traefik/acme.json"
      # Staging server to use during debugging steps
      caServer: "https://acme-v02.api.letsencrypt.org/directory"
      keyType: "RSA4096"
      # DNS challenge is executed through the Gandi provider
      dnsChallenge:
        provider: "gandiv5"
        resolvers:
          - "ns-166-a.gandi.net"
          - "ns-189-b.gandi.net"
          - "ns-89-c.gandi.net"

