# How to spawn a Traefik proxy with docker-compose

## Requirements

* A Docker daemon configured to expose a secured TCP endpoint
* A functional `docker-compose` [CLI](https://docs.docker.com/compose/install/)
* TLS certificates configured to be loaded by the Docker daemon
* Enough permissions to add iptables rules if required (depending on your configuration)

## How to generate the required TLS certificates

Resources used to write the following documentation: [Secure a Docker engine](https://docs.docker.com/engine/security/https/#daemon-modes)

* Generate the TLS CA certificates and client certificates

```bash
openssl genrsa -aes256 -out docker-ca-key.pem 4096
openssl req -new -x509 -days 365 -key docker-ca-key.pem -sha256 -out docker-ca.pem
openssl genrsa -out docker-server-key.pem 4096
# Modify the `domain.tld` value
openssl req -subj "/CN=${DOMAIN}.${TLD}" -sha256 -new -key docker-server-key.pem -out docker-server.csr
# Modify the `domain.tld` value
echo subjectAltName = DNS:${DOMAIN}.${TLD},DNS:localhost,IP:0.0.0.0,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in docker-server.csr -CA docker-ca.pem -CAkey docker-ca-key.pem -CAcreateserial -out docker-server-cert.pem -extfile extfile.cnf
openssl genrsa -out docker-client-key.pem 4096
openssl req -subj '/CN=client' -new -key docker-client-key.pem -out docker-client.csr
echo extendedKeyUsage = clientAuth > extfile-client.cnf
openssl x509 -req -days 365 -sha256 -in docker-client.csr -CA docker-ca.pem -CAkey docker-ca-key.pem -CAcreateserial -out docker-client-cert.pem -extfile extfile-client.cnf
```

* Secure freshly generated files

```bash
chmod -v 0400 /etc/docker/certs/{docker-ca-key.pem,docker-server-key.pem,docker-client-key.pem}
chmod -v 0444 /etc/docker/certs/{docker-ca.pem,docker-server-cert.pem,docker-client-cert.pem}
```

* Disable previous configured environment variables related to the Docker daemon

```bash
unset DOCKER_HOST DOCKER_TLS_VERIFY
```

* Edit the `/etc/docker/daemon.json` file

```json
"hosts": ["tcp://0.0.0.0:2376"],
"tls": true,
"tlsverify": true,
"tlscacert": "/etc/docker/certs/docker-ca.pem",
"tlscert": "/etc/docker/certs/docker-server-cert.pem",
"tlskey": "/etc/docker/certs/docker-server-key.pem"
```

* Then, restart the Docker daemon

```bash
systemctl restart docker.service
```

* Check if a secured connection is working as a Docker client

```bash
docker --tlsverify --tlscacert=/etc/docker/certs/docker-ca.pem --tlscert=/etc/docker/certs/docker-client-cert.pem --tlskey=/etc/docker/certs/docker-client-key.pem info
```

* Move required certificate files into the `.docker` folder

```bash
cp /etc/docker/certs/docker-ca.pem ~/.docker/ca.pem
cp /etc/docker/certs/docker-client-cert.pem ~/.docker/cert.pem
cp /etc/docker/certs/docker-client-key.pem ~/.docker/key.pem
```

## How to communicate with the secured Docker daemon

* Add the following configuration into your Bash profile

```bash
export DOCKER_HOST=tcp://127.0.0.1:2376 DOCKER_TLS_VERIFY=1
```

## Start the Traefik proxy

### Configure the `.env` file

Edit and modify the `.env` file available in the `proxy` folder to feat your environment.  
In this repository, the Traefik component is configured to "automagically" generate the required Let's Encrypt certificates.  
Some parameters are defined in the `proxy/config/traefik-configuration.yml` file to use the [DNS challenge](https://doc.traefik.io/traefik/user-guides/docker-compose/acme-dns/) method with the provider __Gandi.net__.  
To use this project "as is", you need to own a domain registered at Gandi and a functional `GANDIV5_API_KEY` token. But you can modify that depending your needs. 

### Configure the `docker-compose` file

Unless some specific changes, the `proxy/docker-compose.yml` doesn't need to be modified.  
All the required parameters are declared in the `proxy/.env` file which is loaded by the compose file.  

### Setup the required IPtables rules (if required)

* Modify the following variables in the script, then execute it
  - `CONTAINER_CIDR_SUBNET`
  - `DOCKER_DAEMON_TCP_PORT`

```bash
chmod +x iptables-setup.sh
./iptables-setup.sh
```

### Configure your domain name

* Before to start the service, configure your domain name to add the following DNS entry

```text
traefik.${DOMAIN}.{TLD}. 600     IN      A       ${SERVER_IP_ADRESS}
```

### Run the service in background

```bash
cd proxy/
docker-compose -f docker-compose.yml up -d --force-recreate
```

* Open your browser to the following URL

```text
https://traefik.${DOMAIN}.${TLD}:11443/dashboard/#/
```

* Live testing

To live test your environment, you can start a `whoami` service from the `docker-compose` available in the `whoami` directory.  
Declare a new DNS entry like `whoami.${DOMAIN}.${TLD}`, update the `traefik.http.routers.whoami.rule=Host()` label in the `whoami/docker-compose.yml` file.  

Then, start the service:

```bash
# start the service
docker-compose -f whoami/docker-compose.yml up -d
# scale the service just for the fun
docker-compose -f whoami/docker-compose.yml up -d --scale whoami=4
```

* Observe the "hot" reloading configuration of the Traefik proxy and enjoy it!

## Troubleshooting and gardening time

* In case of problem with the ACME configuration file

```bash
touch proxy/acme/acme.json
chmod 600 proxy/acme/acme.json
```

* Debug and live logs

```bash
docker-compose -f /path/to/the/docker-compose.yml logs -f
```

* Remove all deployed resources

```bash
docker-compose -f docker-compose down --remove-orphans
```

