#!/bin/sh
# shellcheck disable=SC1117

#
## @brief: This script add new iptables rules to allow network communication
## between the Traefik Docker container and the TCP Docker endpoint.
#

# Editable variables
CONTAINER_CIDR_SUBNET="172.21.0.1/16"
DOCKER_DAEMON_TCP_PORT="2376"
GLOBAL_RULES_COMMENT="Allow traffic between Traefik container and the TCP Docker endpoint"
DOCKER_SSMTP_TCP_PORT="465"
SSMTP_RULES_COMMENT="Allow traffic between running Docker containers and external submission over TLS providers"

# Rules related to the Docker daemon's configuration
iptables -C INPUT --source ${CONTAINER_CIDR_SUBNET} --destination 0.0.0.0/0 --protocol tcp --dport ${DOCKER_DAEMON_TCP_PORT} -j ACCEPT -m comment --comment "${GLOBAL_RULES_COMMENT}" || \
iptables -A INPUT --source ${CONTAINER_CIDR_SUBNET} --destination 0.0.0.0/0 --protocol tcp --dport ${DOCKER_DAEMON_TCP_PORT} -j ACCEPT -m comment --comment "${GLOBAL_RULES_COMMENT}"
iptables -C OUTPUT --source 0.0.0.0/0 --destination ${CONTAINER_CIDR_SUBNET} --protocol tcp --dport ${DOCKER_DAEMON_TCP_PORT} -j ACCEPT -m comment --comment "${GLOBAL_RULES_COMMENT}"|| \
iptables -A OUTPUT --source 0.0.0.0/0 --destination ${CONTAINER_CIDR_SUBNET} --protocol tcp --dport ${DOCKER_DAEMON_TCP_PORT} -j ACCEPT -m comment --comment "${GLOBAL_RULES_COMMENT}"

# Rules defined to allow running containers to contact some external SMTP providers
iptables -C INPUT --source ${CONTAINER_CIDR_SUBNET} --destination 0.0.0.0/0 --protocol tcp --dport ${DOCKER_SSMTP_TCP_PORT} -j ACCEPT -m comment --comment "${SSMTP_RULES_COMMENT}" || \
iptables -A INPUT --source ${CONTAINER_CIDR_SUBNET} --destination 0.0.0.0/0 --protocol tcp --dport ${DOCKER_SSMTP_TCP_PORT} -j ACCEPT -m comment --comment "${SSMTP_RULES_COMMENT}"
iptables -C OUTPUT --source 0.0.0.0/0 --destination ${CONTAINER_CIDR_SUBNET} --protocol tcp --dport ${DOCKER_SSMTP_TCP_PORT} -j ACCEPT -m comment --comment "${SSMTP_RULES_COMMENT}"|| \
iptables -A OUTPUT --source 0.0.0.0/0 --destination ${CONTAINER_CIDR_SUBNET} --protocol tcp --dport ${DOCKER_SSMTP_TCP_PORT} -j ACCEPT -m comment --comment "${SSMTP_RULES_COMMENT}"

# Print configured chains
printf "%s\n" "INPUT chains control"
iptables -nL INPUT --line-numbers | grep -E "${DOCKER_DAEMON_TCP_PORT}|${DOCKER_SSMTP_TCP_PORT}"
printf "%s\n" "OUTPUT chains control"
iptables -nL OUTPUT --line-numbers | grep -E "${DOCKER_DAEMON_TCP_PORT}|${DOCKER_SSMTP_TCP_PORT}"

